var mongoose = require('mongoose');
var Schema =mongoose.Schema;

var postDataSchema=new Schema({
    title:{type:String,require:true},
    data:{type:String,require:true},
    createdAt:{type:Date,default:Date.now},
    author:String,
    published:{type:Boolean,default:false}
});
module.exports=mongoose.model('Post',postDataSchema);
